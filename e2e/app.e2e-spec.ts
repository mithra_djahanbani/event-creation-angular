import { EventCreationRestPage } from './app.po';

describe('event-creation-rest App', () => {
  let page: EventCreationRestPage;

  beforeEach(() => {
    page = new EventCreationRestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
